# SwitchTable #

Easy HTML tables as a mountable rails engine

### Goals ###

* Provide searching, sorting, and display configuration (default and user settings)
* Provide support for modern HTML transpilers
* Provide support for CSS frameworks

### How do I get set up? ###

* Pull down the project
* Bundle

### Contribution guidelines ###

* Attach kolichmf to pull requests for review
* I will decline your pull requests if there are no tests

### Test Suite ###

* Uses rspec-rails for syntactic sugar
* Uses factory_girl_rails for mocking
* Uses guard for picking up local changes and automating the test runner
* Uses ruby_gntp for displaying growl notifications

### Working with rspec and guard ###

* Open terminal and cd into SwitchTable
* Run `guard`
* Make changes to any spec/controller/model
* Guard will automatically run the specs for the associated file if they exist
* or press "enter" in the guard CLI to run all specs
* or run `rspec`
* that's it!