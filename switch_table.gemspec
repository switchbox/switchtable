$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "switch_table/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "switch_table"
  s.version     = SwitchTable::VERSION
  s.authors     = ["kolichmf"]
  s.email       = ["mkolich@switchboxinc.com"]
  s.homepage    = "TODO"
  s.summary     = "TODO: Summary of SwitchTable."
  s.description = "TODO: Description of SwitchTable."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", "~> 4.2.4"

  s.add_development_dependency "sqlite3"
end
